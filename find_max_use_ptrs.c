#include<stdio.h>

#define MAX 100

int main()
{
    int arr[MAX], *ptr, *mx, i, n;

    n=7;

    for(i=0;i<n;i++)
    {
        printf("Enter a number\n");
        scanf("%d", &arr[i]);
    }

    ptr=arr;
    mx=arr;

    for(i=1;i<n;i++,ptr++)
    {
        if(*ptr > *mx)
        {
            mx=ptr;
        }
    }
    printf("Max Number in Array = %d", *mx);

}
