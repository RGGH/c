// Example of struct inside a struct

#include<stdio.h>


typedef struct {

    int age;
    int engine;
    char *name;

} vehicle;

// struct "vehicle" is inside struct "motor" 
typedef struct {

    vehicle acar;
    int wheels;

} motor;

// main 
int main(){

    vehicle mycar;

    mycar.age = 4;
    mycar.engine = 1.5;
    mycar.name = "AsjtrA";
    printf("%s", mycar.name);

    motor anycar;
    
    anycar.acar.age = 44;
    anycar.wheels = 4;
    printf("%d", anycar.wheels);
    printf("%d", anycar.acar.age);
}
