
/* Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1]
*/

#include <stdio.h>

void twoSum(int *nums, int n, int target)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (nums[i] + nums[j] == target)
            {
                printf("%d, %d", i, j);
                return;
            }
        }
    }
}

int main()
{

    int nums[] = {2, 4, 7, 11};
    int target = 9;

    // get the number of items in the array
    int n = sizeof(nums) / sizeof(nums[0]);

    twoSum(nums, n, target);
    return 0;
}
