// use pointer to track max value in array

#include<stdio.h>

#define max 100

int main()
{
    int *ptr, mx, i, n;
    n=7;
    int p[]={3,11,7,7,8,8};
    mx = 0;
    ptr = p;

    for(i=0;i<n;i++)
    {
        if (mx < *ptr)
        {
            mx = *ptr;
        }
        ptr++;
    }
    printf("%d",mx);

}
