/* Credit Prof Barry Brown 
https://www.youtube.com/watch?v=nFuaTsJUpy8&t=1528s */

#include <stdio.h>

// get length of string including any extra spaces
int lenstr(char *s)
{
    int counter = 0;
    while (s[counter] != '\0')
    {
        counter++;
    }
    return counter;
}

// void trim off whitespaces
void trim(char *s)
{

    while(*s) s++;

    while(*s ==' ' || *s=='\0' || *s=='\n') s--;

    *(s+1)='\0';
}

int main()
{

    char str[80];
    printf("Enter string : ");
    fgets(str,100, stdin);
    printf("Length of string = %d\n", lenstr(str));
    trim(str);
    printf(">%s<\n", str);
}
